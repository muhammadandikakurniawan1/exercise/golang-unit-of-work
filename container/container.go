package container

import (
	"app/unitofwork"
	"app/usecase/account"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Container struct {
	AccountUsecase account.AccountUsecase
}

func NewContainer() *Container {

	// repo
	db, err := sqlx.Connect("postgres", "user=hakim password=masuk123 dbname=test_unitofwork sslmode=disable")
	if err != nil {
		log.Panic(err)
	}

	// unit of work
	accountUow := unitofwork.NewAccountUnitOfWork(db)

	// usecase
	accountUsecase := account.NewAccountUsecase(accountUow)

	return &Container{
		AccountUsecase: accountUsecase,
	}
}
