package main

import (
	"app/container"
	"app/delivery/http"
)

func main() {
	container := container.NewContainer()
	http.Run(container)
}
