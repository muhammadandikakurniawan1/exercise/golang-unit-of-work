package entity

const (
	MutationTableName  = "mutation"
	InsertMutaionQuery = `INSERT INTO ` + MutationTableName + ` (from_user_id, to_user_id, title, amount, created_at) VALUES ($1,$2,$3,$4,$5) RETURNING id`
)

type MutationEntity struct {
	Id         int64
	ToUserId   string
	FromUserId string
	CreatedAt  int64
	Title      string
	Amount     float64
}
