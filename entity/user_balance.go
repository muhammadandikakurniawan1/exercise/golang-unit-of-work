package entity

const (
	UserBalanceTableName                = "user_balance"
	UpdateBalanceByUserIdQuery          = `UPDATE ` + UserBalanceTableName + ` SET amount = $1, updated_at = $2 WHERE user_id = $3`
	GetUserBalanceByUserIdQuery         = `SELECT user_id, amount FROM` + UserBalanceTableName + ` WHERE user_id = $1`
	GetUserBalanceByUserIdWithLockQuery = `SELECT user_id, amount FROM ` + UserBalanceTableName + ` WHERE user_id = $1 LIMIT 1 FOR UPDATE`
	LockUserBalanceQuery                = `LOCK TABLE ` + UserBalanceTableName + ` IN ROW EXCLUSIVE MODE`
)

type UserBalanceEntity struct {
	UserId    string
	Amount    float64
	UpdatedAt int64
}
