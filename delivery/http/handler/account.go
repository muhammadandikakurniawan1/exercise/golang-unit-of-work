package handler

import (
	"app/usecase/account"
	"encoding/json"
	"net/http"
)

func NewAccountHandler(accountUsecase account.AccountUsecase) AccountHandler {
	return AccountHandler{
		accountUsecase: accountUsecase,
	}
}

type AccountHandler struct {
	accountUsecase account.AccountUsecase
}

func (h AccountHandler) Transfer(w http.ResponseWriter, r *http.Request) {

	var requestBody account.TransferRequestModel
	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		return
	}

	w.Header().Set("content-type", "application/json")

	ctx := r.Context()

	err := h.accountUsecase.Transfer(ctx, requestBody)
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		model := map[string]interface{}{
			"status":  "failed",
			"success": false,
			"message": err.Error(),
		}
		json.NewEncoder(w).Encode(model)
		return
	}
	w.WriteHeader(http.StatusOK)
	model := map[string]interface{}{
		"status":  "success",
		"success": true,
		"message": "success",
	}
	json.NewEncoder(w).Encode(model)
	return

}
