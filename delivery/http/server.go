package http

import (
	"app/container"
	"app/delivery/http/handler"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func Run(container *container.Container) {
	router := mux.NewRouter()
	port := "2424"
	appChan := make(chan os.Signal, 1)
	signal.Notify(appChan, os.Interrupt, syscall.SIGTERM)

	// ================================== START SETUP HANDLER ==================================
	accountHandler := handler.NewAccountHandler(container.AccountUsecase)
	// ================================== END SETUP HANDLER ==================================

	// =================================== AUTH ROUTE ========================================
	accountHandlerRoute := router.PathPrefix("/account").Subrouter()
	accountHandlerRoute.HandleFunc("/transfer", accountHandler.Transfer).Methods(http.MethodPut)

	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*", "http://localhost:3000/"},
		AllowedMethods: []string{"*", "GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"*", "x-api-key"},
		// ExposedHeaders:   []string{},
		AllowCredentials: true,
	})

	handler := cors.Handler(router)

	go func() {
		log.Printf("HTTP running on port %s.", port)
		log.Println(http.ListenAndServe(fmt.Sprintf(":%s", port), handler))
	}()

	<-appChan
	log.Println("HTTP SERVER CLOSED")
}
