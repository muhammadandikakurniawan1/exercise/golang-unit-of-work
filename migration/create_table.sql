CREATE TABLE user_balance
(
    user_id character varying(50),
    amount double precision,
    updated_at bigint DEFAULT 0
)

CREATE TABLE mutation
(
    id SERIAL PRIMARY KEY,
    title character varying(500),
    from_user_id character varying(50),
    to_user_id character varying(50),
    amount double precision,
    created_at bigint DEFAULT 0
)