package repo

import (
	"app/entity"
	"context"
)

type UserBalanceRepo interface {
	UpdateAmount(ctx context.Context, userId string, amount float64) (affectedRow int64, err error)
	GetBalanceByUserId(ctx context.Context, userId string) (result entity.UserBalanceEntity, err error)
	GetBalanceByUserIdWithLock(ctx context.Context, userId string) (result entity.UserBalanceEntity, err error)
	LockTable(ctx context.Context) (err error)
}

type MutationRepo interface {
	Insert(ctx context.Context, param *entity.MutationEntity) (err error)
}
