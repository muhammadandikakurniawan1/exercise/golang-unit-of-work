package repo

import (
	"app/database"
	"app/entity"
	"context"
	"time"
)

func NewUserBalanceRepo(db database.Database) UserBalanceRepo {
	return &userBalanceRepoPostgreSQL{
		db: db,
	}
}

type userBalanceRepoPostgreSQL struct {
	db database.Database
}

func (repo userBalanceRepoPostgreSQL) UpdateAmount(ctx context.Context, userId string, amount float64) (affectedRow int64, err error) {
	updatedAt := time.Now().UnixMilli()
	queryArgs := []interface{}{amount, updatedAt, userId}
	res, err := repo.db.ExecContext(ctx, entity.UpdateBalanceByUserIdQuery, queryArgs...)
	if err != nil {
		return
	}
	return res.RowsAffected()
}

func (repo userBalanceRepoPostgreSQL) GetBalanceByUserId(ctx context.Context, userId string) (result entity.UserBalanceEntity, err error) {
	err = repo.db.QueryRowContext(ctx, entity.GetUserBalanceByUserIdQuery, userId).Scan(&result.UserId, &result.Amount)
	return
}

func (repo userBalanceRepoPostgreSQL) LockTable(ctx context.Context) (err error) {
	_, err = repo.db.ExecContext(ctx, entity.LockUserBalanceQuery)
	return
}

func (repo userBalanceRepoPostgreSQL) GetBalanceByUserIdWithLock(ctx context.Context, userId string) (result entity.UserBalanceEntity, err error) {

	row := repo.db.QueryRowContext(ctx, entity.GetUserBalanceByUserIdWithLockQuery, userId)
	if err = row.Err(); err != nil {
		return
	}

	err = row.Scan(&result.UserId, &result.Amount)
	return
}
