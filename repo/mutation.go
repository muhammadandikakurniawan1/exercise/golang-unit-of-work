package repo

import (
	"app/database"
	"app/entity"
	"context"
	"time"
)

func NewMutationRepo(db database.Database) MutationRepo {
	return &mutationRepoPostgreSQL{
		db: db,
	}
}

type mutationRepoPostgreSQL struct {
	db database.Database
}

func (repo mutationRepoPostgreSQL) Insert(ctx context.Context, param *entity.MutationEntity) (err error) {
	if param == nil {
		return
	}
	if param.CreatedAt <= 0 {
		param.CreatedAt = time.Now().UnixMilli()
	}
	queryArgs := []interface{}{param.FromUserId, param.ToUserId, param.Title, param.Amount, param.CreatedAt}
	err = repo.db.QueryRowContext(ctx, entity.InsertMutaionQuery, queryArgs...).Scan(&param.Id)
	return
}
