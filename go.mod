module app

go 1.18

require (
	github.com/jmoiron/sqlx v1.3.5
	golang.org/x/sync v0.2.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.24.0 // indirect
)

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/lib/pq v1.10.9
	github.com/rs/cors v1.9.0
)
