package account

type TransferRequestModel struct {
	SenderUserId   string  `json:"sender_user_id"`
	ReceiverUserId string  `json:"receiver_user_id"`
	SendedAmount   float64 `json:"amount"`
	Title          string  `json:"title"`
}
