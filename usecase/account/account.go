package account

import (
	"app/entity"
	"app/unitofwork"
	"context"
	"errors"
	"log"
	"time"

	"golang.org/x/sync/errgroup"
)

type AccountUsecase interface {
	Transfer(ctx context.Context, request TransferRequestModel) (err error)
}

func NewAccountUsecase(
	accountUow unitofwork.AccountUnitOfWork,
) AccountUsecase {
	return &accountUsecaseImpl{
		accountUow: accountUow,
	}
}

type accountUsecaseImpl struct {
	accountUow unitofwork.AccountUnitOfWork
}

func (uc accountUsecaseImpl) Transfer(ctx context.Context, request TransferRequestModel) (err error) {
	// start validation
	if request.SendedAmount <= 0 {
		return errors.New("invalid amount")
	}
	// end validation

	accountUow, err := uc.accountUow.BeginTx(ctx)
	if err != nil {
		return
	}
	defer accountUow.RollbackTx(ctx)

	if err = accountUow.UserBalanceRepo().LockTable(ctx); err != nil {
		return
	}

	eg := errgroup.Group{}

	var (
		senderData   entity.UserBalanceEntity
		receiverData entity.UserBalanceEntity
	)

	// eg.Go(func() (err error) {
	senderData, err = accountUow.UserBalanceRepo().GetBalanceByUserIdWithLock(ctx, request.SenderUserId)
	if err != nil {
		return
	}
	if senderData.UserId == "" {
		err = errors.New("sender not found")
		return
	}

	restSenderBalance := senderData.Amount - request.SendedAmount
	balanceIsNotSuccifient := restSenderBalance < 0
	if balanceIsNotSuccifient {
		err = errors.New("balance is not sufficient")
		return
	}
	senderData.Amount = restSenderBalance
	// 	return
	// })

	// eg.Go(func() (err error) {
	receiverData, err = accountUow.UserBalanceRepo().GetBalanceByUserIdWithLock(ctx, request.ReceiverUserId)
	if err != nil {
		return
	}
	if receiverData.UserId == "" {
		err = errors.New("receiver not found")
		return
	}
	receiverData.Amount += request.SendedAmount
	// 	return
	// })

	// if err = eg.Wait(); err != nil {
	// 	return
	// }

	// update balance

	eg.Go(func() (egErr error) {
		affectedRow, egErr := accountUow.UserBalanceRepo().UpdateAmount(ctx, senderData.UserId, senderData.Amount)
		if egErr != nil {
			return
		}
		if affectedRow <= 0 {
			egErr = errors.New("failed update sender balance")
		}
		return
	})

	eg.Go(func() (egErr error) {
		affectedRow, egErr := accountUow.UserBalanceRepo().UpdateAmount(ctx, receiverData.UserId, receiverData.Amount)
		if egErr != nil {
			return
		}
		if affectedRow <= 0 {
			egErr = errors.New("failed update receiver balance")
		}
		return
	})

	eg.Go(func() (egErr error) {
		// create mutation
		mutationData := entity.MutationEntity{
			Title:      request.Title,
			FromUserId: senderData.UserId,
			ToUserId:   receiverData.UserId,
			Amount:     request.SendedAmount,
			CreatedAt:  time.Now().UnixMilli(),
		}
		if egErr = accountUow.MutationRepo().Insert(ctx, &mutationData); egErr != nil {
			return
		}
		return
	})

	if err = eg.Wait(); err != nil {
		return
	}

	if err = accountUow.CommitTx(ctx); err != nil {
		errMsg := "internal server error, failed transfer process"
		log.Println(errMsg, err)
		err = errors.New("internal server error, failed transfer process")
	}
	return
}
