package unitofwork

import (
	"app/repo"
	"context"
	"errors"

	"github.com/jmoiron/sqlx"
)

func NewAccountUnitOfWork(db *sqlx.DB) AccountUnitOfWork {

	return &AccountUnitOfWorkImpl{
		db: db,

		mutationRepo:    repo.NewMutationRepo(db),
		userBalanceRepo: repo.NewUserBalanceRepo(db),
	}

}

func newAccountUnitOfWorkTx(tx *sqlx.Tx) AccountUnitOfWork {

	return &AccountUnitOfWorkImpl{
		txDb: tx,

		mutationRepo:    repo.NewMutationRepo(tx),
		userBalanceRepo: repo.NewUserBalanceRepo(tx),
	}

}

type AccountUnitOfWork interface {
	BeginTx(ctx context.Context) (AccountUnitOfWork, error)
	RollbackTx(ctx context.Context) error
	CommitTx(ctx context.Context) error

	// repositories
	MutationRepo() repo.MutationRepo
	UserBalanceRepo() repo.UserBalanceRepo
}

type AccountUnitOfWorkImpl struct {
	db   *sqlx.DB
	txDb *sqlx.Tx

	mutationRepo    repo.MutationRepo
	userBalanceRepo repo.UserBalanceRepo
}

func (u AccountUnitOfWorkImpl) BeginTx(ctx context.Context) (instance AccountUnitOfWork, err error) {
	if u.txDb != nil {
		err = errors.New("transaction already open")
		return
	}
	if u.db == nil {
		err = errors.New("connection not found")
		return
	}

	tx, err := u.db.BeginTxx(ctx, nil)
	if err != nil {
		return
	}

	instance = newAccountUnitOfWorkTx(tx)
	return
}

func (u AccountUnitOfWorkImpl) RollbackTx(ctx context.Context) error {
	if u.txDb == nil {
		return nil
	}
	return u.txDb.Rollback()
}

func (u AccountUnitOfWorkImpl) CommitTx(ctx context.Context) error {
	if u.txDb == nil {
		return nil
	}
	return u.txDb.Commit()
}

// repositories
func (u AccountUnitOfWorkImpl) MutationRepo() repo.MutationRepo {
	return u.mutationRepo
}

func (u AccountUnitOfWorkImpl) UserBalanceRepo() repo.UserBalanceRepo {
	return u.userBalanceRepo
}
